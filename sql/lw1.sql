
--
-- Database: `lw1`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `nome` varchar(30) NOT NULL,
  `sobrenome` varchar(50) NOT NULL,
  `email` varchar(80) NOT NULL,
  `telefone` varchar(16) NOT NULL,
  `senha` varchar(256) NOT NULL,
  `last_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `tipo_usuario` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `user`
--

INSERT INTO `user` (`id`, `nome`, `sobrenome`, `email`, `telefone`, `senha`, `last_modified`, `tipo_usuario`) VALUES
(1, 'José ', 'Henrique', 'asdasd@fadfas.com', '54879874654', 'aaaaaaaaaaaaaa', '2019-05-07 22:45:43', 'aluno'),
(2, 'Pedro ', 'Pereira', 'pedropereira@pedro.com', '400258922', 'pedropereira', '2019-05-07 22:45:43', 'aluno'),
(6, 'aaaaaaa', 'aaaaaa', 'aaa@aaa.aaa', 'aaaaaaaa', 'aaaaaaaa', '2019-05-07 22:57:16', 'aluno'),
(7, 'bbbbb', 'bbbbb', 'bbb@bbb.bbb', '', 'bbbbbbbb', '2019-05-08 01:02:08', 'profe'),
(8, 'ccccc', 'ccccc', 'ccc@ccc.ccc', '', 'cccccccc', '2019-05-08 01:03:14', 'admin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;




--
-- Estrutura da tabela `disciplina`
--

CREATE TABLE `disciplina` (
  `id` int(11) NOT NULL,
  `nome` varchar(50) NOT NULL,
  `imagem` varchar(100) NOT NULL,
  `descricao` text NOT NULL,
  `last_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `disciplina`
--

INSERT INTO `disciplina` (`id`, `nome`, `imagem`, `descricao`, `last_modified`) VALUES
(1, 'Python', '/assets/curso/01.jpg', 'A melhor linguagem de programação que já existiu', '2019-06-12 17:09:28');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `disciplina`
--
ALTER TABLE `disciplina`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `disciplina`
--
ALTER TABLE `disciplina`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;






--
-- Estrutura da tabela `aula`
--

CREATE TABLE `aula` (
  `id` int(11) NOT NULL,
  `titulo` varchar(50) NOT NULL,
  `conteudo` text NOT NULL,
  `anexo` varchar(100) NOT NULL,
  `disciplina` varchar(50) NOT NULL,
  `last_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `aula`
--

INSERT INTO `aula` (`id`, `titulo`, `conteudo`, `anexo`, `disciplina`, `last_modified`) VALUES
(2, 'aaaaaaaaab', 'aaaaaaaaaaaaaaaaaaaaaab', 'aaaaaaaaaaaab', 'Python', '2019-06-12 18:54:12');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `aula`
--
ALTER TABLE `aula`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `aula`
--
ALTER TABLE `aula`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;