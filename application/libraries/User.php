<?php

class User{
    private $nome;
    private $sobrenome;
    private $senha;
    private $email;
    private $tipo_usuario;
    
    private $telefone;

    private $db;

    function __construct($nome=null, $sobrenome=null, $email=null, $senha=null, $tipo_usuario=null){
        $this->nome = $nome;
        $this->email= $email;
        $this->senha = $senha;
        $this->sobrenome = $sobrenome;
        $this->tipo_usuario = $tipo_usuario;

        $ci = &get_instance();
        $this->db = $ci->db;
    }

    public function setTelefone($telefone){
        $this->telefone = $telefone;
    }

    public function save(){
        $sql = "INSERT INTO user (nome, email, senha, sobrenome, telefone, tipo_usuario) 
        VALUES ('$this->nome', '$this->email', '$this->senha', '$this->sobrenome', '$this->telefone', '$this->tipo_usuario')"; 
        $this->db->query($sql);
    }

    public function getALL(){
        $sql = "SELECT * FROM user";
        $res = $this->db->query($sql);
        return $res->result_array();
    }

    public function getById($id){
        $rs = $this->db->get_where('user', "id = $id");
       return $rs->row_array();
    }

    public function update($data, $id){
        $this->db->update('user', $data, "id = $id");
        return $this->db->affected_rows();
    }

    public function delete($id){
        $this->db->delete('user', "id = $id");
    }

    public function validate(){
        $sql = "SELECT * FROM user WHERE email = '$this->email' AND senha = '$this->senha'";
        $res = $this->db->query($sql);
        return $res->result_array();
    }

}

?>