<?php

class Aula{
    private $titulo;
    private $conteudo;
    private $anexo;

    private $disciplina;
    
    private $db;

    function __construct($titulo=null, $conteudo=null, $anexo=null){
        $this->titulo = $titulo;
        $this->conteudo = $conteudo;
        $this->anexo = $anexo;

        $ci = &get_instance();
        $this->db = $ci->db;
    }

    public function setDisciplina($disciplina){
        $this->disciplina = $disciplina;
    }

    public function save(){
        $sql = "INSERT INTO aula (titulo, conteudo, anexo, disciplina) 
        VALUES ('$this->titulo', '$this->conteudo', '$this->anexo', '$this->disciplina')"; 
        $this->db->query($sql);
    }

    public function getALL($nome){
        $sql = "SELECT * FROM aula WHERE disciplina = '$nome'";
        $res = $this->db->query($sql);
        return $res->result_array();
    }

    public function getById($id){
        $rs = $this->db->get_where('aula', "id = $id");
       return $rs->row_array();
    }

    public function update($data, $id){
        $this->db->update('aula', $data, "id = $id");
        return $this->db->affected_rows();
    }

    public function delete($id){
        $this->db->delete('aula', "id = $id");
    }

}

?>