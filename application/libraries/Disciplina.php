<?php

class Disciplina{
    private $nome;
    private $imagem;
    private $descricao;
    
    private $db;

    function __construct($nome=null, $imagem=null, $descricao=null){
        $this->nome = $nome;
        $this->imagem= $imagem;
        $this->descricao = $descricao;

        $ci = &get_instance();
        $this->db = $ci->db;
    }

    public function save(){
        $sql = "INSERT INTO disciplina (nome, imagem, descricao) 
        VALUES ('$this->nome', '$this->imagem', '$this->descricao')"; 
        $this->db->query($sql);
    }

    public function getALL(){
        $sql = "SELECT * FROM disciplina";
        $res = $this->db->query($sql);
        return $res->result_array();
    }

    public function getById($id){
        $rs = $this->db->get_where('disciplina', "id = $id");
       return $rs->row_array();
    }

    public function update($data, $id){
        $this->db->update('disciplina', $data, "id = $id");
        return $this->db->affected_rows();
    }

    public function delete($id){
        $this->db->delete('disciplina', "id = $id");
    }

}

?>