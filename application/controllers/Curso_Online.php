<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Curso_Online extends CI_Controller {

	public function index(){
                session_start();

                $this->load->view('common/header');
                $this->load->view('common/navbar');

                $this->load->view('curso/home');

                $this->load->view('common/page_footer');
                $this->load->view('common/footer');
        }

        public function disciplinas(){
                session_start();

                $this->load->view('common/header');
                $this->load->view('common/navbar');

                $this->load->model('DisciplinaModel', 'model');
                $data['disciplinas'] = $this->model->showDisc();
                $this->load->view('curso/view_disciplina', $data);

                $this->load->view('common/page_footer');
                $this->load->view('common/footer');
        }

        public function aulas($id){
                session_start();

                $this->load->view('common/header');
                $this->load->view('common/navbar');

                $this->load->model('AulaModel', 'model');
                $data['nome'] = $this->model->carrega_disc_nome($id);
                $data['aulas'] = $this->model->showAula($data['nome']);
                $this->load->view('curso/view_aula', $data);

                $this->load->view('common/page_footer');
                $this->load->view('common/footer');
        }

        public function listar_disc(){
                session_start();
                if(!isset($_SESSION['nome']) || empty($_SESSION['nome'])){
                        redirect('usuario/login');
                }

                $this->load->view('common/header');
                $this->load->view('common/navbar');

                $this->load->model('DisciplinaModel', 'model');
                $v['lista'] = $this->model->lista();
                $this->load->view('disciplina/table_view', $v);

                $this->load->view('common/page_footer');
                $this->load->view('common/footer');
        }

        public function listar_aula($id){
                session_start();
                $_SESSION['disc'] = $id;

                $this->load->view('common/header');
                $this->load->view('common/navbar');

                $this->load->model('AulaModel', 'model');
                $v['nome'] = $this->model->carrega_disc_nome($id);
                $v['lista'] = $this->model->lista($id, $v['nome']);
                $this->load->view('aula/table_view', $v);

                $this->load->view('common/page_footer');
                $this->load->view('common/footer');
        }

        public function criar_disc(){
                session_start();

                $this->load->view('common/header');
                $this->load->view('common/navbar');

                $this->load->model('DisciplinaModel', 'model');
                $this->model->criar();
                $data['titulo'] = "Cadastro"; 
                $data['acao'] = "Enviar"; 
                $this->load->view('disciplina/form_cadastro', $data);

                $this->load->view('common/page_footer');
                $this->load->view('common/footer');
        }

        public function criar_aula($id){
                session_start();

                $this->load->view('common/header');
                $this->load->view('common/navbar');

                $this->load->model('AulaModel', 'model');
                $nome = $this->model->carrega_disc_nome($id);
                $this->model->criar($nome, $id);
                $data['titulo'] = "Cadastro"; 
                $data['acao'] = "Enviar"; 
                $this->load->view('aula/form_cadastro', $data);

                $this->load->view('common/page_footer');
                $this->load->view('common/footer');
        }

        public function editar_disc($id){
                session_start();

                $this->load->view('common/header');
                $this->load->view('common/navbar');

                $this->load->model('DisciplinaModel', 'model');
                $this->model->atualizar($id);

                $data['titulo'] = "Edição da Disciplina"; 
                $data['acao'] = "Atualizar"; 
                $data['disc'] = $this->model->carrega_disc($id);
                $this->load->view('disciplina/form_cadastro', $data);

                $this->load->view('common/page_footer');
                $this->load->view('common/footer');
        }

        public function editar_aula($id){
                session_start();

                $this->load->view('common/header');
                $this->load->view('common/navbar');

                $this->load->model('AulaModel', 'model');
                $this->model->atualizar($id);

                $data['titulo'] = "Edição da Aula"; 
                $data['acao'] = "Atualizar"; 
                $data['aula'] = $this->model->carrega_aula($id);
                $this->load->view('aula/form_cadastro', $data);

                $this->load->view('common/page_footer');
                $this->load->view('common/footer');
        }

        public function deletar_disc($id){
                session_start();

                $this->load->model('DisciplinaModel', 'model');
                $this->model->delete($id);
                redirect('curso_online/listar_disc');
        }

        public function deletar_aula($id){
                session_start();

                $this->load->model('AulaModel', 'model');
                $this->model->delete($id);
                redirect($_SERVER['HTTP_REFERER']);
        }






}
?>