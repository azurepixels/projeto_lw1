<?php
defined('BASEPATH') OR exit('No direct script access allowed');

 class Usuario extends CI_Controller{
     public function admin(){
        session_start();
        if(!isset($_SESSION['nome']) || empty($_SESSION['nome'])){
            redirect('usuario/login');
        }
        $this->load->view('common/header');
        $this->load->view('common/navbar');
       
        $this->load->model('UsuarioModel', 'model');
        $v['lista'] = $this->model->lista();
        $this->load->view('usuario/table_view', $v);

        $this->load->view('common/footer');
     }

    public function cadastro(){
        session_start();
        $this->load->view('common/header');
        $this->load->view('common/navbar');

        $this->load->model('UsuarioModel', 'model');
        $this->model->criar();
        $data['titulo'] = "Cadastro"; 
        $data['acao'] = "Enviar"; 
        $this->load->view('usuario/form_cadastro', $data);

        $this->load->view('common/footer');
    }

    
    public function edit($id){
        session_start();
        $this->load->view('common/header');
        $this->load->view('common/navbar');

        $this->load->model('UsuarioModel', 'model');
        $this->model->atualizar($id);

        $data['titulo'] = "Edição do usuario"; 
        $data['acao'] = "Atualizar"; 
        $data['user'] = $this->model->carrega_usuario($id);
        $data['tipo'] = 1;

        $this->load->view('usuario/form_cadastro', $data);
        $this->load->view('common/footer');
    }

    public function delete($id){
        session_start();
        $this->load->model('UsuarioModel', 'model');
        $this->model->delete($id);
        redirect('usuario/admin');
    }

    public function login(){
        session_start();

        if (isset($_SESSION['nome'])){
            session_destroy();
        }

        $this->load->view('common/header');
        $this->load->view('common/navbar');
        
        $this->load->model('UsuarioModel', 'model');
        $this->model->logar();
        $this->load->view('usuario/form_login');

        $this->load->view('common/footer');
    }

    public function perfil(){
        session_start();
        if(!isset($_SESSION['nome']) || empty($_SESSION['nome'])){
            redirect('usuario/login');
        }
        echo 'perfil';
        $this->load->view('common/header');
        $this->load->view('common/navbar');
        $this->load->view('common/page_footer');
        $this->load->view('common/footer');
    }
 }

?>