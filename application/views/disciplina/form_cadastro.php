<div class="container">
    <div class="row mt-4">
        <div class="col-md-8 mx-auto">
            <form class="text-center border border-light p-5" method="POST">

                <p class="h4 mb-4"><?= $titulo ?></p>

                <input type="text" value="<?= isset($disc['nome']) ? $disc['nome'] : '' ?>" name="nome" id="nome" class="form-control mt-3" placeholder="Nome">

                <input type="text"  value="<?= isset($disc['imagem']) ? $disc['imagem'] : '/assets/curso/' ?>" name="imagem" id="imagem" class="form-control mt-3" placeholder="Imagem">

                <textarea class="form-control rounded-0 mt-3" name="descricao" id="descricao" placeholder="Descrição"  rows="6"><?= isset($disc['descricao']) ? $disc['descricao'] : '' ?></textarea>

                <button class="btn btn-info my-4 btn-block" type="submit"><?= $acao ?></button>

            </form>        
        </div>
    </div>
</div>
