<div class="container">
    <div class="row mt-4">
        <div class="col-md-8 mx-auto">
            <form class="text-center border border-light p-5" method="POST">

                <p class="h4 mb-4"><?= $titulo ?></p>

                <input type="text" value="<?= isset($aula['titulo']) ? $aula['titulo'] : '' ?>" name="titulo" id="titulo" class="form-control mt-3" placeholder="Título">

                <textarea class="form-control rounded-0 mt-3" name="conteudo" id="conteudo" placeholder="Conteúdo"  rows="10"><?= isset($aula['conteudo']) ? $aula['conteudo'] : '' ?></textarea>

                <input type="text"  value="<?= isset($aula['anexo']) ? $aula['anexo'] : '' ?>" name="anexo" id="anexo" class="form-control mt-3" placeholder="Anexo">

                <button class="btn btn-info my-4 btn-block" type="submit"><?= $acao ?></button>

            </form>        
        </div>
    </div>
</div>
