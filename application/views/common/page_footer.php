<!-- Footer -->
<footer class="page-footer font-small blue">

  <!-- Copyright -->
  <div class="footer-copyright text-center py-3">2019 - Danton Ievenes Ferraz - GU1601601<br> 
    Instituto Federal de Educação, Ciência e Tecnologia de São Paulo - Câmpus Guarulhos
  </div>
  <!-- Copyright -->

</footer>
<!-- Footer -->