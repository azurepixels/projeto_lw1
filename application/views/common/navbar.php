<!--Navbar-->
<nav class="navbar navbar-expand-lg sticky-top navbar-dark primary-color">

  <!-- Navbar brand -->
  <a class="navbar-brand" href="<?= base_url('curso_online') ?>">Curso Online</a>

  <!-- Collapse button -->
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#basicExampleNav"
    aria-controls="basicExampleNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <!-- Collapsible content -->
  <div class="collapse navbar-collapse" id="basicExampleNav">

    <!-- Links -->
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="<?= base_url('curso_online') ?>" >Home
          <span class="sr-only">(current)</span>
        </a>
      </li>
      
      <li class="nav-item">
        <a class="nav-link" href="<?= base_url('curso_online/disciplinas') ?>">Disciplinas</a>
      </li>

    </ul>

    <ul class="navbar-nav ml-auto nav-flex-icons">
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink-333" data-toggle="dropdown" aria-haspopup="true"
          aria-expanded="false">
          <i class="fas fa-user"></i>
        </a>
        <div class="dropdown-menu dropdown-menu-right dropdown-default" aria-labelledby="navbarDropdownMenuLink-333">
        <?php
          if (!isset($_SESSION['nome']) || empty($_SESSION['nome'])) {
            echo '<a class="dropdown-item" href="'.base_url("usuario/login").'">Login</a>
            <a class="dropdown-item" href="'.base_url("usuario/cadastro").'">Cadastre-se</a>';
          }
          else{
            echo '<a class="dropdown-item" href="'.base_url("usuario/perfil").'">'.$_SESSION['nome'].'</a>';
            if($_SESSION['tipo_usuario'] == 'admin'){
              echo '<a class="dropdown-item" href="'.base_url("usuario/admin").'">Painel Administrativo</a>';
            }
            else if($_SESSION['tipo_usuario'] == 'profe'){
              echo '<a class="dropdown-item" href="'.base_url("curso_online/listar_disc").'">Painel de Cursos</a>';
            }
            echo '<a class="dropdown-item" href="'.base_url("usuario/login").'">Logout</a>';
          }
        ?>
        </div>
      </li>
    </ul>

  </div>
  <!-- Collapsible content -->

</nav>
<!--/.Navbar-->