<!-- Jumbotron -->
<div class="card card-image" style="background-image: url(https://mdbootstrap.com/img/Photos/Others/forest2.jpg);">
  <div class="text-white text-center rgba-stylish-strong py-5 px-4">
    <div class="py-5">

      <!-- Content -->
      <h2 class="card-title h2 my-4 py-2">Cursos Online</h2>
      <p class="mb-4 pb-2 px-md-5 mx-md-5">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur obcaecati vero aliquid libero doloribus ad, unde tempora maiores, ullam, modi qui quidem minima debitis perferendis vitae cumque et quo impedit.</p>
      <a class="btn peach-gradient" href="<?= base_url('curso_online/disciplinas') ?>"><i class="fas fa-clone left"></i>Ver Cursos</a>

    </div>
  </div>
</div>
<!-- Jumbotron -->