<?php

defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH.'libraries/Aula.php';
include_once APPPATH.'libraries/Disciplina.php';

 class AulaModel extends CI_Controller{

    public function carrega_aula($id){
        $aula = new Aula();
        return $aula->getById($id);
    }
    
    public function carrega_disc_nome($id){
        $disc = new Disciplina();
        $data = $disc->getById($id);
        return $data['nome'];
    }

    public function criar($nome, $id){
        if(sizeof($_POST) == 0) return;

        $titulo = $this->input->post('titulo');
        $conteudo = $this->input->post('conteudo');
        $anexo = $this->input->post('anexo');
        $aula = new Aula($titulo, $conteudo, $anexo);
        $aula->setDisciplina($nome);
        $aula->save();
        redirect('curso_online/listar_aula/'.$id);
    }
    
    public function atualizar($id){
        if(sizeof($_POST) == 0) return;

        $data = $this->input->post();
        $aula = new Aula();
        if($aula->update($data, $id))
            redirect('curso_online/listar_aula/'.$_SESSION['disc']);
        
    }


    public function lista($id, $nome){
        $html = '';
        $aula = new Aula();
        $data = $aula->getAll($nome);
        $html .= '<table class="table">';
        foreach($data as $row){
            $html .= '<tr>';
            $html .= '<td>'.$row['titulo'].'</td>';
            $html .= '<td>'.$row['conteudo'].'</td>';
            $html .= '<td>'.$row['anexo'].'</td>';
            $html .= '<td>'.$row['disciplina'].'</td>';
            $html .= '<td>'.$this->get_edit_icons($row['id']).'</td></tr>';

        }
        $html .= '<tr>';
        $html .= '<td>';
        $html .= '<a href="'.base_url("curso_online/criar_aula/$id").'"><button type="button" class="btn btn-primary">Criar Aula</button></a>';
        $html .= '<a href="'.base_url("curso_online/listar_disc").'"><button type="button" class="btn btn-primary ml-2">Voltar</button></a>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        return $html;
    }

    public function showAula($nome){

        $html = '<div class="row">
                    <div class="card-deck mt-4 col-md-10">';

        $aula = new Aula();
        $data = $aula->getALL($nome);

        foreach ($data as $row) {
            $html .= '<div class="col-md-5">
                    <div class="card mb-4">
                        <div class="card-body">
                            <h4 class="card-title">'.$row['titulo'].'</h4>
                            <p class="card-text">'.$row['conteudo'].'</p>
                            <div class="text-center">
                                <a href="'.$row['anexo'].'" class="btn btn-primary">Ver Anexo</a>
                            </div>
                        </div>
                    </div>
                </div>';
        }

        $html .= '</div>
                </div>';
        
        return $html;
    }

    private function get_edit_icons($id){
        $html = '';
        $html .= '<a href="'.base_url('curso_online/editar_aula/'.$id).'"><i class="far fa-edit mr-3 text-primary"></i></a>';
        $html .= '<a href="'.base_url('curso_online/deletar_aula/'.$id).'"><i class="far fa-trash-alt text-danger"></i></a>';
        return $html;
    }

    public function delete($id){
        $aula = new Aula();
        $aula->delete($id);    
    }
}

?>