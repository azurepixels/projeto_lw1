<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH.'libraries/User.php';

 class UsuarioModel extends CI_Controller{

    public function carrega_usuario($id){
        $user = new User();
        return $user->getById($id);
    }

    public function criar(){
        if(sizeof($_POST) == 0) return;

        $nome = $this->input->post('nome');
        $sobrenome = $this->input->post('sobrenome');
        $email = $this->input->post('email');
        $senha = $this->input->post('senha');
        $tipo_usuario = 'aluno';
        $user = new User($nome, $sobrenome, $email, $senha, $tipo_usuario);
        $user->setTelefone($this->input->post('telefone'));
        $user->save();
        redirect('curso_online');
    }
    
    public function atualizar($id){
        if(sizeof($_POST) == 0) return;

        $data = $this->input->post();
        $user = new User();
        if($user->update($data, $id))
            redirect('usuario/admin');
        
    }


    public function lista(){
        $html = '';
        $user = new User();
        $data = $user->getAll();
        $html .= '<table class="table">';
        foreach($data as $row){
            $html .= '<tr>';
            $html .= '<td>'.$row['nome'].'</td>';
            $html .= '<td>'.$row['sobrenome'].'</td>';
            $html .= '<td>'.$row['email'].'</td>';
            $html .= '<td>'.$row['telefone'].'</td>';
            $html .= '<td>'.$row['tipo_usuario'].'</td>';
            $html .= '<td>'.$this->get_edit_icons($row['id']).'</td></tr>';

        }
        $html .= '</table>';
        return $html;
    }

    private function get_edit_icons($id){
        $html = '';
        $html .= '<a href="'.base_url('usuario/edit/'.$id).'"><i class="far fa-edit mr-3 text-primary"></i></a>';
        $html .= '<a href="'.base_url('usuario/delete/'.$id).'"><i class="far fa-trash-alt text-danger"></i></a>';
        return $html;
    }

    public function delete($id){
        $user = new User();
        $user->delete($id);    
    }

    public function logar(){
        $email = $this->input->post('email');
        $senha = $this->input->post('senha');
        $user = new User($nome = NULL, $sobrenome = NULL, $email, $senha, $tipo_usuario = NULL);
        $login = $user->validate();
        if($login != NULL){

            session_start();
            $_SESSION['id'] = $login[0]['id'];
            $_SESSION['nome'] = $login[0]['nome']; 
            $_SESSION['tipo_usuario'] = $login[0]['tipo_usuario'];

            if($login[0]['tipo_usuario'] == 'admin'){
                redirect('usuario/admin');
            }
            else if($login[0]['tipo_usuario'] == 'profe'){
                echo 'página de disciplinas e aulas';
                redirect('curso_online/listar_disc');
            }
            else{
                redirect('curso_online');
            };
        }
    }
    


 }

?>