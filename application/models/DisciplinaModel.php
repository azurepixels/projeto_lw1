<?php

defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH.'libraries/Disciplina.php';

 class DisciplinaModel extends CI_Controller{

    public function carrega_disc($id){
        $disc = new Disciplina();
        return $disc->getById($id);
    }

    public function criar(){
        if(sizeof($_POST) == 0) return;

        $nome = $this->input->post('nome');
        $imagem = $this->input->post('imagem');
        $descricao = $this->input->post('descricao');
        $disc = new Disciplina($nome, $imagem, $descricao);
        $disc->save();
        redirect('curso_online/listar_disc');
    }
    
    public function atualizar($id){
        if(sizeof($_POST) == 0) return;

        $data = $this->input->post();
        $disc = new Disciplina();
        if($disc->update($data, $id))
            redirect('curso_online/listar_disc');
        
    }


    public function lista(){
        $html = '';
        $disc = new Disciplina();
        $data = $disc->getAll();
        $html .= '<table class="table">';
        foreach($data as $row){
            $html .= '<tr>';
            $html .= '<td><a href="'.base_url("curso_online/listar_aula/".$row['id']).'">'.$row['nome'].'</a></td>';
            $html .= '<td>'.$row['imagem'].'</td>';
            $html .= '<td>'.$row['descricao'].'</td>';
            $html .= '<td>'.$this->get_edit_icons($row['id']).'</td></tr>';

        }
        $html .= '<tr>';
        $html .= '<td><a href="'.base_url("curso_online/criar_disc").'"><button type="button" class="btn btn-primary">Criar Disciplina</button></a></td>';
        $html .= '</tr>';
        $html .= '</table>';
        return $html;
    }

    public function showDisc(){

        $html = '<div class="row">
                    <div class="card-deck mt-4 col-md-10">';

        $disc = new Disciplina();
        $data = $disc->getALL();

        foreach ($data as $row) {
            $html .= '<div class="col-md-5">
                    <div class="card mb-4">
                        <img class="card-img-top" src="'.base_url($row['imagem']).'">
                        <div class="card-body">
                            <h4 class="card-title">'.$row['nome'].'</h4>
                            <p class="card-text">'.$row['descricao'].'</p>
                            <div class="text-center">
                                <a href="'.base_url('curso_online/aulas/'.$row['id']).'" class="btn btn-primary">Ver Aulas</a>
                            </div>
                        </div>
                    </div>
                </div>';
        }

        $html .= '</div>
                </div>';
        
        return $html;
    }

    private function get_edit_icons($id){
        $html = '';
        $html .= '<a href="'.base_url('curso_online/editar_disc/'.$id).'"><i class="far fa-edit mr-3 text-primary"></i></a>';
        $html .= '<a href="'.base_url('curso_online/deletar_disc/'.$id).'"><i class="far fa-trash-alt text-danger"></i></a>';
        return $html;
    }

    public function delete($id){
        $disc = new Disciplina();
        $disc->delete($id);    
    }
}

?>